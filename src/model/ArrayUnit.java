package model;

import java.util.Arrays;

/**
 * Created by isild on 03.04.2017.
 */
public class ArrayUnit {
    private  UnifiedModel[] unifiedModelArray = new UnifiedModel[10];


    public ArrayUnit(){

    }

    public void add(UnifiedModel newElement) {
        unifiedModelArray = addElement( unifiedModelArray, newElement);
    }

    private UnifiedModel[] addElement(UnifiedModel[] a, UnifiedModel e) {
        a = Arrays.copyOf(a, a.length*2/3 + 1);
        a[a.length - 1] = e;
        return a;
    }

    public UnifiedModel getByName(UnifiedModel input, String name){


        if (input.getName().equals(name))
        {
            return input;
        }

        else{System.out.println("ne to");}

        return null;
    }

    public UnifiedModel getByParam(String key,ItemParam itemParam, UnifiedModel unifiedModel){

        if (itemParam.getKey().equals(key))
        {
            return unifiedModel;

        }

        else{System.out.println("ne to");}


return null;
    }

    public UnifiedModel getByValue(String value,ItemParam itemParam, UnifiedModel unifiedModel){

    if (itemParam.getValue().equals(value))
    {
        return unifiedModel;

    }

    else{System.out.println("ne to");}


    return null;
}




    public ArrayUnit(UnifiedModel[] unifiedModelArray) {
        this.unifiedModelArray = unifiedModelArray;
    }

    public UnifiedModel[] getUnifiedModelArray() {
        return unifiedModelArray;
    }

    public void setUnifiedModelArray(UnifiedModel[] unifiedModelArray) {
        this.unifiedModelArray = unifiedModelArray;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ArrayUnit arrayUnit = (ArrayUnit) o;

        // Probably incorrect - comparing Object[] arrays with Arrays.equals
        return Arrays.equals(unifiedModelArray, arrayUnit.unifiedModelArray);

    }

    @Override
    public int hashCode() {
        return unifiedModelArray != null ? Arrays.hashCode(unifiedModelArray) : 0;
    }

    @Override
    public String
    toString() {
        return "ArrayUnit{" +
                "unifiedModelArray=" + Arrays.toString(unifiedModelArray) +
                '}';
    }
}
