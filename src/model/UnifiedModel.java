package model;

import enums.Category;

import java.util.Arrays;

/**
 * Created by isild on 03.04.2017.
 */
public class UnifiedModel {

    private String name;

    private Category category;

    private ItemParam[] unitParam;

    public  UnifiedModel(){}

    public UnifiedModel(String name, Category category, ItemParam[] unitParam) {
        this.name = name;
        this.category = category;
        this.unitParam = unitParam;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public ItemParam[] getUnitParam() {
        return unitParam;
    }

    public void setUnitParam(ItemParam[] unitParam) {
        this.unitParam = unitParam;
    }



    public void print(){
        String s = name+" ("+category +")\n";
        for(int i = 0 ; i < unitParam.length;i++){
            s+=" "+unitParam[i]+"\n";
        }
        System.out.println(s);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UnifiedModel that = (UnifiedModel) o;

        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (category != that.category) return false;
        // Probably incorrect - comparing Object[] arrays with Arrays.equals
        return Arrays.equals(unitParam, that.unitParam);

    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (category != null ? category.hashCode() : 0);
        result = 31 * result + (unitParam != null ? Arrays.hashCode(unitParam) : 0);
        return result;
    }

    @Override
    public String toString() {
        return "UnifiedModel{" +
                "name='" + name + '\'' +
                ", category=" + category +
                ", unitParam=" + Arrays.toString(unitParam) +
                '}';
    }
}
